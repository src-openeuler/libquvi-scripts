%global debug_package %{nil}

name:		libquvi-scripts
Version:	0.9.20131130
Release:	18
Summary:	Lua scripts for parsing the media properties
License:	AGPLv3+
URL:		http://quvi.sourceforge.net
Source0:	https://netix.dl.sourceforge.net/project/quvi/0.9/%{name}/%{name}-%{version}.tar.xz

BuildRequires:	gcc asciidoc
Requires:	lua-expat lua-socket lua-json

%description
libquvi-scripts contains the Lua scripts for libquvi that it uses to parse
the media properties.

%package	help
Summary: 	Doc files for %{name}
Buildarch:	noarch
Requires:	man

%description 	help
The %{name}-help package contains doc files for %{name}.

%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --with-nsfw
%make_build

%install
%make_install pkgconfigdir=%{_datadir}/pkgconfig/

%pre

%preun

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%license AUTHORS COPYING
%{_datadir}/%{name}
%{_datadir}/pkgconfig/%{name}*.pc

%files help
%doc NEWS ChangeLog README
%{_mandir}/*/*

%changelog
* Mon Aug 02 2021 chenyanpanHW <chenyanpan@huawei.com> - 0.9.20131130-18
- DESC: delete -S git from %autosetup, and delete BuildRequires git

* Fri Jul 23 2021 shangyibin<shangyibin1@huawei.com> - 0.9.20131130-17
- Type:bugfix
- ID: NA
- SUG: NA
- DESC:delete gdb build dependency

* Thu Dec 10 2020 shixuantong<shixuantong@huawei.com> - 0.9.20131130-16
- Type: NA
- ID: NA
- SUG: NA
- DESC:update source0

* Thu Oct 15 2020 shixuantong <shixuantong@huawei.com> - 0.9.20131130-15
- Type: NA
- ID: NA
- SUG: NA
- DESC:update source0

* Wed Jan 8 2020 lvying <lvying6@huawei.com> - 0.9.20131130-14
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: update patch

* Fri Sep 27 2019 luhuaxin <luhuaxin@huawei.com> - 0.9.20131130-13
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: move AUTHORS and COPYING files to license folder

* Tue Sep 24 2019 luhuaxin <luhuaxin@huawei.com> - 0.9.20131130-12
- Type: enhancement
- ID: NA
- SUG: NA
- DESC: add help package

* Fri Aug 16 2019 luhuaxin <luhuaxin@huawei.com> - 0.9.20131130-11
- Package init
